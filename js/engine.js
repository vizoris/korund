$(function() {


  jQuery('.catalog-item img, .catalog-menu img').each(function(){
    var $img = jQuery(this);
    var imgID = $img.attr('id');
    var imgClass = $img.attr('class');
    var imgURL = $img.attr('src');

    jQuery.get(imgURL, function(data) {
        // Get the SVG tag, ignore the rest
        var $svg = jQuery(data).find('svg');

        // Add replaced image's ID to the new SVG
        if(typeof imgID !== 'undefined') {
            $svg = $svg.attr('id', imgID);
        }
        // Add replaced image's classes to the new SVG
        if(typeof imgClass !== 'undefined') {
            $svg = $svg.attr('class', imgClass+' replaced-svg');
        }

        // Remove any invalid XML tags as per http://validator.w3.org
        $svg = $svg.removeAttr('xmlns:a');

        // Replace image with new SVG
        $img.replaceWith($svg);

    }, 'xml');

});


// Фиксированная шапка припрокрутке
$(window).scroll(function(){

  if ($(window).width() > 768) {
      var sticky = $('.header'),
          scroll = $(window).scrollTop();
      if (scroll > 100) {
          sticky.addClass('header-fixed');
      } else {
          sticky.removeClass('header-fixed');
      };
    }
});


$(window).resize(function() {
  if ($(window).width() <= 768) {
      $('.header').removeClass('header-fixed'); 
    }

});



// achievements slider
$('.banner').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      fade: true,
      // autoplay: true,
      // autoplaySpeed:4000,
      arrows: true,
      dots: true,
     
  });

$('.advantages-slider').slick({
      infinite: true,
      slidesToShow: 4,
      slidesToScroll: 4,
      // autoplay: true,
      // autoplaySpeed:4000,
      arrows: true,
      dots: true,
      appendDots: $(".advantages-slider__dots"),
      prevArrow: $(".advantages-slider__prev"),
      nextArrow: $(".advantages-slider__next"),
        responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        }
      },
       {
        breakpoint: 550,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
      ]

     
  });




// Табы
$('.tabs-nav a').click(function() {
    var _id = $(this).attr('href');
    var _targetElement = $(this).parents('.tabs-wrap').find('#' + _id);
    $(this).parents('.tabs-nav').find('a').removeClass('active');
    $(this).addClass('active');
     $(this).parents('.tabs-wrap').children('.tabs-item__wrap').children('.tabs-item').removeClass('active');
    _targetElement.addClass('active');
    return false;

});


$('.tabs-mobile__btn').click(function() {
    var _targetElementParent = $(this).parent('.tabs-item');
    _targetElementParent.toggleClass('active-m');
    _targetElementParent.find('.tabs-item__body').slideToggle(600);
    $('.tabs-mobile__btn').parent('.tabs-item').removeClass('active');
    return false;
});


$('.tabs-radio-nav a').click(function() {
    var _id = $(this).attr('href');
    var _targetElement = $(this).parents('.tabs-radio-wrap').find('#' + _id);
    $(this).parents('.tabs-radio-nav').find('a').removeClass('active');
    $(this).addClass('active');
     $(this).parents('.tabs-radio-wrap').find('.tabs-item').removeClass('active');
    _targetElement.addClass('active');
    return false;

});


// Плейсфолдер в инпутах
const FloatLabel = (() => {
  
  // add active class and placeholder 
  const handleFocus = (e) => {
    const target = e.target;
    target.parentNode.classList.add('active');
    
  };
  
  // remove active class and placeholder
  const handleBlur = (e) => {
    const target = e.target;
    if(!target.value) {
      target.parentNode.classList.remove('active');
    }
     
  };  
  
  // register events
  const bindEvents = (element) => {
    const floatField = element.querySelector('input,textarea');
    floatField.addEventListener('focus', handleFocus);
    floatField.addEventListener('blur', handleBlur);    
  };
  
  // get DOM elements
  const init = () => {
    const floatContainers = document.querySelectorAll('.form-item');
    
    floatContainers.forEach((element) => {
      if (element.querySelector('input,textarea').value) {
          element.classList.add('active');
      }      
      
      bindEvents(element);
    });
  };
  
  return {
    init: init
  };
})();

FloatLabel.init();








// Показать подробности/Скрыть подробности
$('.history-more__show').click(function(event) {
    $(this).parent().parent().next('.history-table__row--hidden').toggleClass('active');
    event.preventDefault();
    this.textContent = this.textContent === 'Смотреть состав заказа' ? 'Скрыть состав заказа' : 'Смотреть состав заказа';
});










// Выпадающее меню
  if ($(window).width() > 1199) {
    $('.drop').hover(function() {
      $(this).children('.drop-menu').stop(true, true).toggleClass('active');
    })
}

// Показать/скрыть корзину при наведении
$('.header-cart').hover(function() {
  $(this).toggleClass('active');
  $('.header-cart__block').stop(true, true).fadeToggle();
})


$('.priority-menu > li').hover(function() {
  $('.priority-menu > li').removeClass('active');
  $('.priority-menu__dropdown').removeClass('active');
  $(this).addClass('active');
  $(this).children('.priority-menu__dropdown').stop(true, true).addClass('active');
})





// Показать\скрыть фильтры

const btnFilters = $('.banner-wrap .btn-filter');
const btnText = $('.banner-wrap .btn-filter span');

btnFilters.click(function() {
  $('.filters').fadeToggle('active');
  console.log($(this).children('span').text());
  $(this).children('span').innerHTML = 
  (btnText.text() === 'Показать фильтр') ? btnText.text('Скрыть фильтр')  : btnText.text('Показать фильтр');
})





 // Стилизация селектов
$('select').styler();




// FAQ
$('.faq-item__header').click(function(){
  if(!($(this).next().hasClass('active'))){
    $('.faq-item__body').slideUp().removeClass('active');
    $(this).next().slideDown().addClass('active');
  }else{
    $('.faq-item__body').slideUp().removeClass('active');
  };

  if(!($(this).parent('.faq-item').hasClass('active'))){
    $('.faq-item').removeClass("active");
    $(this).parent('.faq-item').addClass("active");
  }else{
    $(this).parent('.faq-item').removeClass("active");
  };
});







// Филтры в сайдбаре
$('.sidebar-item__header').click(function() {
  $(this).addClass('active');
  $(this).next('.sidebar-item__body').fadeToggle();
})


// сайдбар на мобильном
$('.sidebar-mobile__btn').click(function() {
  $(this).toggleClass('active');
  $('.sidebar').toggleClass('active')
})





var productSlider = $('.card-img__slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  dots: false,
  
  asNavFor: '.card-img__slider--nav',

});


$('.card-img__slider--nav').slick({
  slidesToShow: 5,
  slidesToScroll: 1,
  asNavFor: '.card-img__slider',
  dots: false,
  arrows: true,
  focusOnSelect: true,
  vertical: true,

  responsive: [
  {
    breakpoint: 768,
    settings: {
      vertical: false,
    }
  },
  {
    breakpoint: 479,
    settings: {
      slidesToShow: 3,
      vertical: false,
    }
  },
  ]
});















// Прикрепляем файлы
$('.attach').each(function() { // на случай, если таких групп файлов будет больше одной
  var attach = $(this),
    fieldClass = 'attach__item', // класс поля
    attachedClass = 'attach__item--attached', // класс поля с файлом
    fields = attach.find('.' + fieldClass).length, // начальное кол-во полей
    fieldsAttached = 0; // начальное кол-во полей с файлами

  var newItem = '<div class="attach__item"><label><div class="attach__up">Добавить файл</div><input class="attach__input" type="file" name="files[]" /></label><div class="attach__delete"><i class="icon icon-cross"></i></div><div class="attach__name"></div><div class="attach__edit">Изменить</div></div>'; // разметка нового поля

  // При изменении инпута
  attach.on('change', '.attach__input', function(e) {
    var item = $(this).closest('.' + fieldClass),
      fileName = '';
    if (e.target.value) { // если value инпута не пустое
      fileName = e.target.value.split('\\').pop(); // оставляем только имя файла и записываем в переменную
    }
    if (fileName) { // если имя файла не пустое
      item.find('.attach__name').text(fileName); // подставляем в поле имя файла
      if (!item.hasClass(attachedClass)) { // если в поле до этого не было файла
        item.addClass(attachedClass); // отмечаем поле классом
        fieldsAttached++;
      }
      if (fields < 10 && fields == fieldsAttached) { // если полей меньше 10 и кол-во полей равно
        item.after($(newItem)); // добавляем новое поле
        fields++;
      }
    } else { // если имя файла пустое
      if (fields == fieldsAttached + 1) {
        item.remove(); // удаляем поле
        fields--;
      } else {
        item.replaceWith($(newItem)); // заменяем поле на "чистое"
      }
      fieldsAttached--;

      if (fields == 1) { // если поле осталось одно
        attach.find('.attach__up').text('Прикрепить файл'); // меняем текст
      }
    }
  });

  // При нажатии на "Изменить"
  attach.on('click', '.attach__edit', function() {
    $(this).closest('.attach__item').find('.attach__input').trigger('click'); // имитируем клик на инпут
  });

  // При нажатии на "Удалить"
  attach.on('click', '.attach__delete', function() {
    var item = $(this).closest('.' + fieldClass);
    if (fields > fieldsAttached) { // если полей больше, чем загруженных файлов
      item.remove(); // удаляем поле
      fields--;
    } else { // если равно
      item.after($(newItem)); // добавляем новое поле
      item.remove(); // удаляем старое
    }
    fieldsAttached--;
    if (fields == 1) { // если поле осталось одно
      attach.find('.attach__up').text('Прикрепить файл'); // меняем текст
    }
  });
});










// FansyBox
 $('.fancybox').fancybox({
  closeExisting: true,
 });


// Маска телефона
$(".phone-mask").mask("+7 (999) 99-99-999");



































// MMENU
if ($(window).width() < 992) {
var menu = new MmenuLight(
  document.querySelector( '#mobile-menu' ),
  'all'
);

var navigator = menu.navigation({
  // selectedClass: 'Selected',
  // slidingSubmenus: true,
  // theme: 'dark',
   title: 'Меню'
});

var drawer = menu.offcanvas({
  // position: 'left'
});

//  Open the menu.
document.querySelector( 'a[href="#mobile-menu"]' )
  .addEventListener( 'click', evnt => {
    evnt.preventDefault();
    drawer.open();
});


}else {
  //Показать/скрыть каталог
$('.catalog-btn').click(function(event) {
  event.preventDefault();
  $(this).toggleClass('active');
  $('.catalog-menu').fadeToggle();
});
}







// Begin of slider-range
$( "#slider-range" ).slider({
  range: true,
  min: 100,
  max: 10000,
  values: [ 2000, 5600 ],
  slide: function( event, ui ) {
    $( "#amount" ).val( ui.values[ 0 ] );
    $( "#amount-1" ).val( ui.values[ 1 ] );
  }
});
$( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 ) );
$( "#amount-1" ).val( $( "#slider-range" ).slider( "values", 1 ) );

      // Изменение местоположения ползунка при вводиде данных в первый элемент input
      $("input#amount").change(function(){
        var value1=$("input#amount").val();
        var value2=$("input#amount-1").val();
        if(parseInt(value1) > parseInt(value2)){
          value1 = value2;
          $("input#amount").val(value1);
        }
        $("#slider-range").slider("values",0,value1); 
      });
      
      // Изменение местоположения ползунка при вводиде данных в второй элемент input  
      $("input#amount-1").change(function(){
        var value1=$("input#amount").val();
        var value2=$("input#amount-1").val();

        if(parseInt(value1) > parseInt(value2)){
          value2 = value1;
          $("input#amount-1").val(value2);
        }
        $("#slider-range").slider("values",1,value2);
      });
// END of slider-range



// Меню в футере на мобильном
if ($(window).width() <= 992) {
  $('.footer-links__item .h4').click(function() {
    $(this).next('ul').fadeToggle();
    $(this).toggleClass('active');
  })

}






$('.form-item__select').click(function() {
  $(this).next('.form-item__select').find('.form-item__select--label').fadeToggle(0);
})






})